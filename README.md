# Advent of Code

I will post all of my solutions for the advent of code event here.

Link to the contest: https://adventofcode.com/

## Year 2020
This year I decided to practice a language I've been learning this semester in school - Scala.

| **Day**      | **Stars** |
|:------------:|:---------:|
| 1            | ⭐⭐        | 
| 2            | ⭐⭐        | 
| 3            | ⭐⭐        |
| 4            | ⭐⭐        | 
| 5            | ⭐⭐        |
| 6            |           |
| 7            |           |
| 8            |           |
| 9            |           |
| 10           |           |
| 11           |           |
| 12           |           |
| 13           |           |
| 14           |           |
| 15           |           |
| 16           |           |
| 17           |           |
| 18           |           |
| 19           |           |
| 20           |           |
| 21           |           |
| 22           |           |
| 23           |           |
| 24           |           |
| 25           |           |

## Year 2019
I decided to take the opportunity and learn a new language solving these puzzles.

| **Day**      | **Stars** |
|:------------:|:---------:|
| 1            | ⭐⭐       | 
| 2            | ⭐⭐       | 
| 3            |           |
| 4            | ⭐⭐       | 
| 5            |           |
| 6            | ⭐⭐       |
| 7            |           |
| 8            |           |
| 9            |           |
| 10           |           |
| 11           |           |
| 12           |           |
| 13           |           |
| 14           |           |
| 15           |           |
| 16           |           |
| 17           |           |
| 18           |           |
| 19           |           |
| 20           |           |
| 21           |           |
| 22           |           |
| 23           |           |
| 24           |           |
| 25           |           |

